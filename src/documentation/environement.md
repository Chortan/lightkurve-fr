# Environement

!!! info inline end ":fontawesome-brands-linux: Linux"

    Il est tout a fait possible d'installer l'environement sous `Linux`. Mais si vous disposez de Linux c'est que vous êtes débrouillard et que vous n'aurez pas besoin d'instruction pour la mise en place de l'environement :stuck_out_tongue_winking_eye:

Nous allons voir au travèrs de cette page comment mettre en place l'environement de travail sous :fontawesome-brands-windows: `Windows 10` qui permettra d'avoir tous les outis de base permettant de manipuler les différentes données.



Les manipulation se font à l'aide du langage `Python` et `Jupyter lab`

## Python

Il est important d'installer une version récente de `python >= 3.9` afin d'éviter tous problème de compatibilité.


[:fontawesome-brands-python: Télécharger Python](https://www.python.org/downloads/){ .md-button .md-button--primary target=_blank }

Lors de l'installation n'oubliez pas de cocher la case `Add Python 3.X to PATH`.

![](/images/add-python-path.png)

Ouvrez un terminal :fontawesome-solid-terminal: powershell

```powershell title="Verifiez l'installation"
python
```

vous devez obtenir le résultat suivant :

```
Python 3.9.0 (tags/v3.9.0:9cf6752, Oct  5 2020, 15:34:40) [MSC v.1927 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Entrer `quit()` pour quitter

## Location des fichier

Il est important de créer un dossier ou se trouvera le projet pour ce faire vous pouvez le faire avec votre explorateur de fichier ou via les commandes suivantes :

```powershell title="Créer le dossier du projet"
New-Item -Type Directory -Force ~\Projets\astronomie
```

## Virtual environement venv

Nous allons mettre en place un `venv` pour le projet.

Le module venv permet de créer des "environnements virtuels" légers avec leurs propres dépendances isolés du reste du système. Chaque environnement virtuel possède son propre binaire Python (qui correspond à la version du binaire qui a été utilisé pour créer cet environnement) et peut avoir son propre ensemble indépendant de paquets Python installés dans ses répertoires de site.

```powershell title="Créer le venv"
cd ~\Projets\astronomie
python -m venv astroenv
```

A chaque fois que vous ouvriez un nouveau terminal il faudra réactiver le virtual environement.

```powershell title="Activer le venv"
cd ~\Projets\astronomie
.\astroenv\Scripts\activate
```

Vous devez avoir `(astroenv)` au début du prompt dans le terminal

## Jupyter

Alors non il ne saihit de notre belle planète planète Jupiter mais belle et bien d'un logiciel. Il permet de créer des notebook très pratique pour la manipulation de données.

```powershell title="Installer jupyter"
pip install jupyterlab notebook
```

```powershell title="Lancer jupyter"
jupyter-lab
```

## Dépendences

Nous allons installer un certain nombre de dépendences. Nous les utiliserons pas forcément tous au début mais cela vous permettra d'avoir tous les prérequis pour la suite.

```powershell title="Installer les dépendances"
pip install lightkurve astropy numpy matplotlib
```