# Lightcurve

!!! info "A propos"

    Par manque de temps, je vais me baser sur la page de [Lightkurve](https://docs.lightkurve.org/tutorials/) et vous en faire un traduction avec mes notes en plus.

## Qu'est ce qu'un objet `LightCurve` ?

Les objets LightCurve sont des objets de données qui encapsulent la luminosité d'une étoile dans le temps. Ils fournissent une série d'opérations communes, par exemple le `folding`, le `binning`, le `plotting`, etc. Il existe une série de sous-classes d'objets LightCurve spécifiques aux télescopes, notamment KeplerLightCurve pour les données Kepler et K2 et TessLightCurve pour les données TESS.

Bien que lightkurve ait été conçu avec Kepler, K2 et TESS en tête, ces objets peuvent être utilisés pour une large gamme de données astronomiques.

Vous pouvez créer un objet `LightCurve` à partir d'un objet `TargetPixelFile` en utilisant la photométrie d'ouverture simple. La photométrie d'ouverture consiste simplement à additionner les valeurs de tous les pixels dans une ouverture prédéfinie, en fonction du temps. En choisissant soigneusement la forme du masque d'ouverture, vous pouvez éviter les contaminants proches ou améliorer la force du signal spécifique que vous essayez de mesurer par rapport au fond.

Pour démontrer, créons une `KeplerLightCurve` à partir d'un `KeplerTargetPixelFile`.

```python title="Jupyter lab"
from lightkurve import search_targetpixelfile

# Premièrement nous ouvrons un Target Pixel File depuis MAST (Mikulski Archive for Space Telescopes)
tpf = search_targetpixelfile('KIC 6922244', author="Kepler", cadence="long", quarter=4).download()

# En suite nous convertissons le target pixel file dans une light curve en utilisant le masque d'ouverture défini par le pipeline.
lc = tpf.to_lightcurve(aperture_mask=tpf.pipeline_mask)
```

Nous avons construit un nouvel objet `KeplerLightCurve` appelé `lc`. Notez que dans ce cas, nous avons passé un masque d'ouverture `aperture_mask` à la méthode `to_lightcurve`. Par défaut c'est l'ouverture du pipeline Kepler qui est utilisé. (Vous pouvez passer votre propre ouverture, qui est un tableau booléen `numpy`.) En additionnant tous les pixels dans l'ouverture, nous avons créé une courbe de lumière de photométrie à ouverture simple (SAP).

`KeplerLightCurve` possède de nombreuses fonctions utiles que vous pouvez utiliser. Comme pour les fichiers de pixels cibles, vous pouvez accéder aux métadonnées très simplement :

```python 
lc.meta['MISSION']
```    

![](/lightkurve-fr/images/results/1.jpg)


```python
lc.meta['QUARTER']
```

![](/lightkurve-fr/images/results/2.jpg)

Et vous avez toujours accès aux attributs de temps et de flux. Dans une courbe de lumière, il n'y a qu'un seul point de flux pour chaque horodatage :

```python
lc.time
```

![](/lightkurve-fr/images/results/3.jpg)

```python
lc.flux
```

![](/lightkurve-fr/images/results/4.jpg)

Vous pouvez également vérifier la mesure du bruit de la précision photométrique différentielle combinée `CDPP` de la courbe de lumière en utilisant la méthode intégrée `estimate_cdpp()` :

```python
lc.estimate_cdpp()
```

![](/lightkurve-fr/images/results/5.jpg)

Nous pouvons maintenant utiliser la fonction `plot` intégrée à l'objet `KeplerLightCurve` pour tracer la série chronologique. Vous pouvez passer à plot tous les mots-clés que vous passeriez normalement à `matplotlib.pyplot.plot`.

```python
%matplotlib inline
lc.plot()
```

![](/lightkurve-fr/images/results/6.jpg)

Il existe un ensemble de fonctions utiles dans les objets `LightCurve` que vous pouvez utiliser pour travailler avec les données. Ces fonctions sont les suivantes :

* `flatten()` : Supprime les tendances à long terme en utilisant un [filtre Savitzky-Golay](https://fr.wikipedia.org/wiki/Algorithme_de_Savitzky-Golay).
* `remove_outliers()` : Supprime les valeurs aberrantes en utilisant un simple écrêtage sigma.
* `remove_nans()` : Supprime les valeurs infinies ou NaN (celles-ci peuvent se produire lors de l'allumage des propulseurs)
* `fold()` : Regroupe les données à une période particulière
* `bin()` : Réduit la résolution temporelle du tableau, en prenant la valeur moyenne dans chaque bin.

```python
flat_lc = lc.flatten(window_length=401)
flat_lc.plot()
```

![](/lightkurve-fr/images/results/7.jpg)

```python
folded_lc = flat_lc.fold(period=3.5225)
folded_lc.plot()
```

![](/lightkurve-fr/images/results/8.jpg)

```python
binned_lc = folded_lc.bin(time_bin_size=0.01)
binned_lc.plot()
```

![](/lightkurve-fr/images/results/9.jpg)


Ou nous pouvons faire tout cela en une seule (longue) ligne !

```python
lc.remove_nans().flatten(window_length=401).fold(period=3.5225).bin(time_bin_size=0.01).plot();
```

![](/lightkurve-fr/images/results/10.jpg)

## Qu'est-ce que les objets TargetPixelFile ?

Les Target Pixel Files (TPFs) sont un fichier commun à Kepler/K2 et à la mission TESS. Ils contiennent des films des données de pixels centrés sur une seule étoile cible.

Les TPFs peuvent être considérés comme des piles d'images, avec une image pour chaque horodatage du télescope. Chaque horodatage est appelé cadence. Ces images sont des "timbres-poste" découpés de l'observation complète pour faciliter leur utilisation.

Les TPFs sont donnés dans des fichiers FITS, sur lesquels vous pouvez lire plus [ici](https://fr.wikipedia.org/wiki/Flexible_Image_Transport_System). Lightkurve comprend des outils qui vous permettent de travailler directement avec ces fichiers de manière simple et intuitive.

Dans ce tutoriel, nous allons couvrir les bases du travail avec les TPFs. Dans lightkurve il y a des classes pour travailler avec chaque mission. Par exemple `KeplerTargetPixelFile` traite les données de la mission Kepler (et K2). `TessTargetPixelFile` traite les données de la mission TESS. Nous allons utiliser un TPF Kepler comme exemple.

To load a `KeplerTargetPixelFile` from a local path or remote url, simply call Lightkurve’s read function using the location of the file as the parameter:

```python
import lightkurve as lk
tpf = lk.read("https://archive.stsci.edu/pub/kepler/target_pixel_files/0069/006922244/kplr006922244-2010078095331_lpd-targ.fits.gz")
```

Vous pouvez également rechercher l'url automatiquement en utilisant la fonction search_targetpixelfile(). Cette fonction recherchera le bon fichier dans l'archive de données MAST qui contient toutes les données de Kepler et K2. Dans ce cas, nous voulons le fichier de pixels cibles avec Kepler ID 6922244 pour le trimestre 4 (les observations de Kepler étaient divisées en trimestres d'une année) :

```python
from lightkurve import search_targetpixelfile
tpf = search_targetpixelfile('KIC 6922244', author="Kepler", quarter=4, cadence="long").download()
```

Vous pouvez également passer le nom de la cible ou ses coordonnées astronomiques comme paramètre à `search_targetpixelFile()`.

Le code ci-dessus a créé une variable nommée tpf qui est un objet Python de type `KeplerTargetPixelFile` :

```python
tpf
```

![](/lightkurve-fr/images/results/11.jpg)

Nous pouvons accéder à de nombreuses métadonnées en utilisant cet objet de manière simple. Par exemple, nous pouvons trouver le nom de la mission et le trimestre où les données ont été prises en tapant ce qui suit :

```python
tpf.meta['MISSION']
```

![](/lightkurve-fr/images/results/12.jpg)

```python
tpf.meta['QUARTER']
```

![](/lightkurve-fr/images/results/13.jpg)

Vous pouvez trouver la liste complète des propriétés dans la [documentation de l'API](https://docs.lightkurve.org/reference/api/lightkurve.KeplerTargetPixelFile.html) sur cet objet.

Les données les plus intéressantes dans un objet `KeplerTargetPixelFile` sont les valeurs de flux et de temps qui donnent accès à la luminosité de la cible observée au fil du temps. Vous pouvez accéder aux horodatages des observations à l'aide de la propriété time :

```python
tpf.time
```

![](/lightkurve-fr/images/results/14.jpg)

Par défaut, le temps est au format `Barycentric Kepler Julian Day` (BKJD), spécifique à Kepler.

Comme il s'agit d'un objet `AstroPy` Time, vous pouvez accéder à des horodatages ISO lisibles par l'homme en utilisant la propriété `time.iso` :

```python
tpf.time.iso
```

![](/lightkurve-fr/images/results/15.jpg)

!!! warning "Attention"

    Ces horodatages sont dans le cadre barycentrique du système solaire (TDB) et n'incluent pas de corrections pour le temps de déplacement de la lumière ou les secondes intercalaires. 

Pour utiliser une échelle de temps différente, telle que le système UTC centré sur la Terre, vous pouvez utiliser les fonctions de conversion d'échelle de temps d'AstroPy. Par exemple :

```python
tpf.time.utc.iso
```

![](/lightkurve-fr/images/results/16.jpg)

Examinons ensuite les données réelles de l'image, qui sont disponibles via la propriété flux :

```python
tpf.flux.shape
```

![](/lightkurve-fr/images/results/17.jpg)

Les données de flux sont un tableau de 4116x5x5 en unités électrons/seconde. Le premier axe est l'axe temporel, et les images elles-mêmes sont de 5 pixels par 5 pixels. Vous pouvez utiliser la méthode plot sur l'objet KeplerTargetPixelFile pour visualiser les données. (Par défaut, ceci montrera une seule cadence des données. Mais vous pouvez passer la cadence que vous voulez regarder au mot-clé frame si vous voulez vérifier un point de flux particulier pour des déclenchements de propulseurs, des rayons cosmiques ou des astéroïdes).

```python
%matplotlib inline
tpf.plot(frame=0)
```

![](/lightkurve-fr/images/results/18.jpg)


```python
tpf.flux[0]
```

Les valeurs présentées dans cette image sont également directement accessibles sous forme de tableau :

![](/lightkurve-fr/images/results/19.jpg)

Vous pouvez utiliser les méthodes normales de `numpy` sur ceux-ci pour trouver la forme, la moyenne etc !

Nous pouvons maintenant transformer ce fichier de pixels cibles en une courbe de lumière, avec une seule valeur de flux pour chaque valeur de temps. Chacun des pixels a une largeur de 4 secondes d'arc. La fonction d'étalement du point (PSF) du télescope fait que la lumière de l'étoile tombe sur plusieurs pixels différents, comme on peut le voir sur l'image ci-dessus. En raison de cet étalement, nous devons additionner de nombreux pixels pour collecter toute la lumière de la source. Pour ce faire, nous additionnons tous les pixels dans une ouverture. Une ouverture est un masque de pixels, où nous ne prenons que les pixels liés à la cible.

Le pipeline Kepler ajoute un masque d'ouverture à chaque fichier de pixels de la cible. Cette ouverture détermine quels pixels sont additionnés pour créer une courbe de lumière 1-D de la cible. Dans certains cas scientifiques, vous pouvez souhaiter créer une ouverture différente. Par exemple, il peut y avoir un contaminant à proximité ou vous pouvez vouloir mesurer l'arrière-plan.

L'ouverture standard du pipeline est facilement accessible dans un objet `KeplerTargetPixelFile` en utilisant `tpf.pipeline_mask`, qui est un tableau booléen :

```python
tpf.pipeline_mask
```

![](/lightkurve-fr/images/results/20.jpg)

Nous pouvons également tracer cette ouverture sur le fichier de pixels de la cible ci-dessus pour voir si le flux de l'étoile est entièrement contenu dans l'ouverture.

```python
tpf.plot(aperture_mask=tpf.pipeline_mask)
```

![](/lightkurve-fr/images/results/21.jpg)

Maintenant que nous avons l'ouverture, nous pouvons créer une courbe de lumière de photométrie à ouverture simple dans le prochain tutoriel.

nfin, notez que vous pouvez inspecter toutes les métadonnées brutes de la cible en regardant l'en-tête du fichier FITS, qui contient des informations sur l'ensemble des données. Nous allons simplement imprimer les 10 premières lignes :

```python
tpf.get_header()[:10]
```

![](/lightkurve-fr/images/results/22.jpg)

Nous pouvons consulter les valeurs de la deuxième extension du fichier fits en accédant à l'objet `AstroPy FITS HDUList`. Par exemple, pour regarder tous les titres des colonnes :

```python
tpf.hdu[1].header['TTYPE*']
```

![](/lightkurve-fr/images/results/23.jpg)


## Création de vos propres courbes de lumière à l'aide de la photométrie d'ouverture

[ouvrir l'original](https://docs.lightkurve.org/tutorials/2-creating-light-curves/2-1-custom-aperture-photometry.html){ .md-button .md-button--primary target=_blank }

### Objectifs d'apprentissage

À la fin de ce tutoriel, vous pourrez :

* Comprendre comment utiliser la photométrie d'ouverture pour transformer une série d'images bidimensionnelles en une série temporelle unidimensionnelle.
* Vous serez capable de déterminer l'ouverture la plus utile pour la photométrie d'une cible Kepler/K2.
* Créer votre propre courbe de lumière pour un seul trimestre/campagne de données Kepler/K2.

### Introduction

Ce tutoriel fait partie d'une série sur la manipulation des données Kepler et K2 avec Astropy et Lightkurve. Comme vous l'avez vu précédemment dans cette série, Lightkurve fournit des utilitaires qui vous permettent de créer votre propre courbe de lumière à partir d'un fichier de pixels d'une cible Kepler ou K2. Dans ce tutoriel, nous examinerons les principes qui sous-tendent le processus d'exécution de la photométrie d'ouverture sur une cible. Puis nous entrerons dans les détails et apprendrons comment personnaliser nos ouvertures pour tirer le meilleur parti de nos données.

A suivre (quand j'aurais le temps 😉) ...